/*! \file  testMZ04.c
 *
 *  \brief Rotate an RGB LED through the color wheel
 *
 *  \author jjmcd
 *  \date January 9, 2016, 3:19 PM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>
#include <math.h>               /* sin function */
#include <mchp_dsp_wrapper.h>   /* Only for definition of pi */

#define LED LATFbits.LATF2      /* Green LED */

/* Highest duty OCxRS value so each color has similar apparent brightness */
#define MAX_RED   0x8000
#define MAX_BLUE  0x4000
#define MAX_GREEN 0x4000

/*! main - Rotate an RGB LED through the color wheel */

/*! Uses the sin of an angle, 0 through 2PI, to adjust the duty cycle
 *  of the red, green and blue components of an RGB LED
 *
 *  Separate green LED toggles based on TMR8
 */
int main(void)
{
  double angle;     /* Angle of rotation in color wheel */
  double scale;     /* Scaling of duty cycle for a particular LED */
  double delta1;    /* Angular offset of green LED from red */
  double delta2;    /* Angular offset of blue LED from red */

  /* Set the LED pins as outputs */
  TRISFbits.TRISF2 = 0;         /* Make green LED pin output */
  TRISDbits.TRISD14 = 0;        /* Make RGB LED blue pin output */
  TRISDbits.TRISD15 = 0;        /* Make RGB LED red pin output */
  TRISFbits.TRISF8 = 0;         /* Make RGB LED green pin output */

  /* Set up timer 3 for the PWM, fast, full resolution
   * This timer will determine the period for all the LED components
   */
  T3CONbits.TCKPS = 0;          /* 1:1 prescale */
  T3CONbits.TCS = 0;            /* Use Internal peripheral clock */
  PR3 = 0xfffe;                 /* Period register */
  T3CONbits.TON = 1;            /* Turn on the timer */

  /* Blue LED - OC3 */
  RPD14Rbits.RPD14R = 0x0b;     /* Remap Output Compare 3 to PORT D bit 14 */
  OC3CONbits.OCM = 6;           /* PWM mode, no fault pin */
  OC3CONbits.OCTSEL = 1;        /* Use timery=TMR3 (datasheet says 5) */
  OC3R = OC3RS = 0x4000;        /* Initialize duty cycle to max for this color */
  OC3CONbits.ON = 1;            /* Turn on the Output Compare 3 peripheral */

  /* Red LED - OC4 */
  RPD15Rbits.RPD15R = 0x0b;     /* Remap Output Compare 4 to PORT D bit 15 */
  OC4CONbits.OCM = 6;           /* PWM mode, no fault pin */
  OC4CONbits.OCTSEL = 1;        /* Use timery=TMR3 (datasheet agrees) */
  OC4R = OC4RS = 0x8000;        /* Initialize duty cycle to max for this color */
  OC4CONbits.ON = 1;            /* Turn on the Output Compare 4 peripheral */

  /* Green LED - OC5 */
  RPF8Rbits.RPF8R = 0x0b;       /* Remap Output Compare 5 to PORT F bit 8 */
  OC5CONbits.OCM = 6;           /* PWM mode, no fault pin */
  OC5CONbits.OCTSEL = 1;        /* Use timery=TMR3 (datasheet agrees) */
  OC5R = OC5RS = 0x4000;        /* Initialize duty cycle to max for this color */
  OC5CONbits.ON = 1;            /* Turn on the Output Compare 5 peripheral */

  /* Set up timer 8/9 for the green LED
   */
  T8CONbits.TCKPS = 7;          /* 1:1 prescale */
  T8CONbits.TCS = 0;            /* Use Internal peripheral clock */
  PR8 = 0xcffe;                 /* Period register */
  T8CONbits.TON = 1;            /* Turn on the timer */
  TMR8 = 0;

  /* Keep this up tirelessly */
  while(1)
    {
      /* Three colors 120 degrees out of phase */
      delta1 = 2.0 * PI / 3.0;  /* Offset of green component from red */
      delta2 = 4.0 * PI / 3.0;  /* Offset of blue component from red */

      /* Loop through angles, small step slows things down enough to see */
      for ( angle=0.0; angle<2.0*PI; angle+=0.00005)
        {
          /* Red LED component */
          scale = (1.0+sin(angle))/2.0;         /* Scale sine to 0..1 */
          OC4RS = (double)(MAX_RED) * scale;    /* fraction of max duty cycle */
          /* Green LED component */
          scale = (1.0+sin(angle+delta1))/2.0;
          OC5RS = (double)(MAX_GREEN) * scale;
          /* Blue LED component */
          scale = (1.0+sin(angle+delta2))/2.0;
          OC3RS = (double)(MAX_BLUE) * scale;

          if ( IFS1bits.T8IF )
            {
              IFS1bits.T8IF = 0;
              LED ^= 1;
            }
        }
    }

  return 0;
}
