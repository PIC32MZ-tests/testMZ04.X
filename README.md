## Test PIC32MZ1024EFG100 with FRCPLL oscillator

FRC oscillator, System PLL, 200 MHz processor clock

Timer 3 is used to set the period of the Output Compare modules

OC3, OC4 and OC5 set the duty cycle of the components of an RGB LED
which is cycled through the color wheel.

The green LED toggles once per revolution
